/**Создать объект студент "студент" и проанализировать его табель.

 Технические требования:

 Создать пустой объект student, с полями name и last name.
 Спросить у пользователя имя и фамилию студента, полученные значения записать в соответствующие поля объекта.
 В цикле спрашивать у пользователя название предмета и оценку по нему. Если пользователь нажмет Cancel
 при n-вопросе о названии предмета, закончить цикл. Записать оценки по всем предметам в свойство студента tabel.
 Посчитать количество плохих (меньше 4) оценок по предметам. Если таких нет, вывести сообщение Студент
 переведен на следующий курс.
 Посчитать средний балл по предметам. Если он больше 7 - вывести сообщение Студенту назначена стипендия.


 */

const student = {
    name: "name",
    lastName: 'lastName',
    tabel: {},
    askName: function () {
        const name = prompt('Enter student`s name', '');
        const lastName = prompt('Enter student`s last name', '');
        this.name = name;
        this.lastName = lastName;
    },
    askSubject: function () {
        while (true) {
            const subject = prompt('Enter a subject', '');
            if (subject == null) {
                break
            }
            const mark = +prompt('Enter a mark', '');
            if (mark == null) {
                break
            }
            this.tabel[subject] = mark;
        }
    },
    countMarks: function () {
        const marks = Object.values(this.tabel);
        let counter = 0;
        let sum = 0;
        marks.forEach((mark) => {
            if (mark < 4) {
                counter++
            }
            sum += mark;
        });
        if (counter === 0) {
            alert('Student is allowed to attend the next course');
        }
        let objLength = Object.keys(this.tabel).length;
        let averageMark = sum / objLength;
        if (averageMark > 7) {
            alert('Student will have a scholarship');
        }
    },
};

student.askName();
student.askSubject();
student.countMarks();



